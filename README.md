Planning #SWAG
===============

Voici le système de consultation de planning que j'ai conçu, 
comme vous pourrez le constater, il n'est pas parfait, d'autres fonctionnalités
seront ajoutées prochainement, ce système m'a permis de (re)découvrir **PHP** orienté objet
de commencer à me familiariser avec le système **AJAX** et le **Javascript** en général.

Si vous constatez des erreurs ou souhaitez ajouter certaines fonctionnalités, n'hésitez pas, 
le but étant d'obtenir l'application qui répondent aux besoins de tous.

Pour l'utiliser sur votre serveur, il vous suffit d'importer le fichier *planning.sql* 
et de modifier le fichier *conf/db.php* afin de l'adapter à votre serveur
(si vous n'êtes pas familier avec la classe PDO, la documentation PHP est très bien faite
http://php.net/manual/fr/class.pdo.php). 

 À faire
=========

 - Passage de **JavaScript** à **jQuery** pour la partie AJAX
 - Passage de la structure actuelle au **MVC** afin d'obtenir un code plus propre et 
 un meilleur "rangement" des différents fichiers
 - Réinitialisation de l'identifiant plus "proprement" (utilisation d'un script maison)

 *Si vous voyez d'autres améliorations possibles, n'hésitez pas à m'en faire part*
